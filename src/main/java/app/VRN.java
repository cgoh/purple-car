package app;

import java.util.Arrays;

public final class VRN {
    private final transient char[] vrn;
    boolean consumed = false;

    public VRN(char[] vrn1){
        this.vrn = vrn1.clone();
        Arrays.fill(vrn1,'0');
    }

    synchronized char[] get() throws RuntimeException {
        if (consumed) {
            throw new RuntimeException("consumed");
        }
        final char[] cloned = this.vrn.clone();
        Arrays.fill(this.vrn, '0');
        consumed = true;
        return cloned;
    }

}
