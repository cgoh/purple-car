package app;

import org.assertj.core.error.ShouldNotHaveDuplicates;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import static org.springframework.web.client.HttpClientErrorException.*;

@RestController
public class CarSaleController {

    /** 
     * A sudo storage to save Cars
     */
    private HashMap<UUID,Car> storage = new HashMap<UUID,Car>();

    @RequestMapping("/")
    public String index() {
        return "Welcome to The Purple Car, an online car sale company";
    }

    @RequestMapping("/add")
    public String addCar(@Valid @RequestBody Car car) throws RuntimeException {
        if(storage.containsKey(car.getCarID())) {
            throw new DuplicateCarException();
        }

        this.storage.put(car.getCarID(),car);
        return "Added car: " + car.toString();
    }

    @RequestMapping("/view/{id}")
    public String viewCarById(@PathVariable("id") Integer id){
        try {
            return this.storage.get(id).toString();
        } catch (IndexOutOfBoundsException e) {
            return "Sorry! I cannot find this car";
        }
    }
}
