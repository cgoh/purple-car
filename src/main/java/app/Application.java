package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        System.out.println("curl -H \"Content-Type:application/json\" -X POST http://localhost:8080/add -d '{\"carID\":1,\"vin\":1, \"model\": \"foo\", \"make\":\"bar\"}");
    }

}
