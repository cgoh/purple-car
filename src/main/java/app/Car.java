package app;

import jdk.nashorn.internal.ir.annotations.Immutable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;


/**
 * Apache Commons Validator package has methods for
 * various validations: https://commons.apache.org/proper/commons-validator/
 */

public final class Car {

    /**
     * A local unique identifier
     */
    @NotNull
    @Immutable
    private final UUID carID;

    /**
     * Brand of the car (e.g. Audi)
     */
    private final String make;

    /**
     * A name used by manufacture to market a range
     * of similar cars (e.g. Q5)
     */
    @Size(max = 60)
    private final String model;

    /**
     * Vehicle Registration Number Sensitive and unique.
     * private String vrn;
     */
    private final VRN vrn;

    /**
     * North America VIN standard 1981
     */
    private final VIN vin;
    /**
     * Photo of the car.
     * private File photo;
     */

    public Car(UUID carID, String vin, String make, String model, char[] vrn) {
        this.carID = carID;
        this.vin = new VIN(vin);
        this.make = make;
        this.model = model;
        this.vrn = new VRN(vrn);
    }

    public UUID getCarID() {
        return this.carID;
    }

    @Override
    public String toString() {
        return this.carID.toString() + " " + this.vin + " " + this.make + " " + this.model;
    }

}
