package app;

import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
public final class VIN {

    @NotNull
    @Pattern(regexp="[A-HJ-NPR-Z0-9]{17}")
    private String vin;

    public VIN(String vin){
        this.vin = vin;
    }
}
